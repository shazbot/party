#!/bin/sh
if [ ""$1 != "restart" ]; then
 sleep 20
else
  killall -q jitsiroomlist
  cat /usr/share/jitsi-meet/rooms.txt >>/usr/share/jitsi-meet/roomslog.txt 2>/dev/null
  cat /usr/share/jitsi-meet/roomslog.txt >>/var/log/jitsi/rooms.log.1 2>/dev/null
  echo "" >/usr/share/jitsi-meet/roomslog.txt
  mv -f /var/log/jitsi/jicofon.log /var/log/jitsi/jicofon.log.1 2>/dev/null
  /root/jitsiroomlist &
fi
/root/jitsiroomlist /usr/share/jitsi-meet/rooms.txt /usr/share/jitsi-meet/roomslog.txt </var/log/jitsi/jicofo.log >>/var/log/jitsi/jicofon.log &